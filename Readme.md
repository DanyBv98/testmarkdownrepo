# Castle Breakers
-----------------------

## Team Members
- Gagiu George-Daniel
- Hanganu Bogdan
- Ifrim Silviu-Marian
- Man Ștefania

## Our mission
Our team was created because we felt the need for better games to exist.  
And now the real reason: We had to do an university project.

You can find our coding guidelines [here](CodingGuidelines.md).

[[embed url=http://www.youtube.com/watch?v=6YbBmqUnoQM]]
