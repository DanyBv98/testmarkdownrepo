# Coding guidelines

## Table of contents
- [Naming](#markdown-header-naming)
    + [Variables](#markdown-header-variables)
    + [Constants](#markdown-header-constants)
    + [Enumerations](#markdown-header-enumerations)
    + [Macros](#markdown-header-macros)
    + [Classes/namespaces](#markdown-header-classesnamespaces)
    + [Class members](#markdown-header-class-members)
- [Bracing Style](#markdown-header-bracing-style)
## Naming

### Variables
Variables should be named using camelCase and should have specifying names.  
For example: `std::string userName;`


### Constants
Constants should be named starting with k and using camel case.  
For example: `const int kCountDirections = 4;`

### Enumerations
Enumeration name and enumeration values names should follow kebab case.  
For example: 
```cpp
enum class StringType
{
  CStyleString = 0,
  ModernString
};
```

### Macros
Macros should not be used so we do not care about their naming.

### Classes/namespaces
Classes and namespaces should follow the kebab case naming style.  
For example:
```cpp
namespace Cars
{
  class FastCar : public Car
  {
  };
}
```

### Class members
Class members should start with an underline and use camel case.  
For example:
```cpp
class User
{
  std::string phoneNumber;
}
```

## Bracing style
Braces should be placed on an empty line.  
For example:
```cpp
for(auto index = 0; index < count; ++index)
{
  doSomething();
}
```
or
```cpp
int doSomething(int something)
{
  //Process 'something' here
  return something;
}
```